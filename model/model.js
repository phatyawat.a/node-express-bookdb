const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/bookStore',{
    useNewUrlParser: true, useUnifiedTopology: true
}).catch(err => console.log(err))

let employeeSchema = new mongoose.Schema({
    img: String,
    name: String,
    author: String,
    company: String,
    type: Number,
    rate: Number,
    price: Number,
})

module.exports = mongoose.model('BookItem',employeeSchema)