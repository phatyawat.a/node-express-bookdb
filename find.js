//step 1
/*find() find all data*/

//step 2 select field in document
/*
  select('field1 field2') select field what want to find
  distinct() select not duplicate document
*/

//step 3 select field before add condition
/*
 where('field').<conditon>() or where( { field: { $operatior: value}} )
 
*/

//step 4 select condition
/*
  equals(value) select equla value
  ne(value) select not equla value
  lt(value) less than
  lte(value) less than or equal
  gt(value) greater than
  gte(value) greater than or equal
  in([v1,v2, ...]) equal to some value in array
  nin([v1,v2, ...]) Not equal to some value in array 


  { field: { $operatior: value}}

*/

//step 5 set method after select collection
/*
  exec() set method for data which return from other method
*/



const Book = require('./model/model')
const mongoose = require('mongoose')
const insert = require('./insert')


function read(request, response){

  Book.find({},{_id:0}).select('name author')
    .exec((err,docs) => {
      
        if(!err){
          console.log(`found ${docs.length} document(s)`);
          console.log('--------------');
          // for (d of docs) {
          //     console.log( "Name: " + d.name);
          //     console.log( "Salary: " + d.salary);
          // }
          let list = docs
          response.setHeader('Content-Type', 'application/json');
          response.end(JSON.stringify(list))
        }
        
    })
}

function operator()
{
    Emp
    .find().select('name salary')
    .where({'salary':{$gte:25000,$lte:30000}})
    .exec((err,docs) => {
        if(!err){
          console.log(`found ${docs.length} document(s)`);
          console.log('--------------');
          for (d of docs) {
              console.log( "Name: " + d.name);
              console.log( "Salary: " + d.salary);
          }
        }
    })
}

//sort('field') sort from selected field
//sort('-field') 

//sort('field').limit('num') limit data length
//sort('field').findOne(condition) find first data which match condition


function sorting(){

    Emp
    .find().sort('-salary').limit(3).exec((err,docs) => {
        if(!err){
          console.log(`found ${docs.length} document(s)`);
          console.log('--------------');
          for (d of docs) {
              console.log( "Name: " + d.name);
              console.log( "Salary: " + d.salary);
          }
        }
    })

}

module.exports.read = read

