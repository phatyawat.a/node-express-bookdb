const Emp = require('./model/model')

function save() {
 
    let doc1 = {
        name: 'James Bond',
        salary: 30000,
        birthday: new Date(1990, 9, 30),
        married: true,
        phones: ['081xxxxxx','069xxxxxxx']
    }
    new Emp(doc1).save(err => {
        if(!err) {
            console.log('doc1 saved');
            
        }
    })
}


function create() {
    let doc2 = {
        name: 'Flint Stoned',
        salary: 25000,
        birthday: new Date(1995, 12, 31),
        married: false,
        phones: ['085xxxxxx']
    }
    Emp.create(doc2, err => {
        if(!err) {
            console.log('doc2 saved');
            insertMany()
        }
    })
}

function insertMany() {
    let docs = [{
        name: 'Jack Jones',
        salary: 40000,
        birthday: new Date(1993, 11, 4),
        married: true,
        phones: ['085xxxxxx']
    },{
        name: 'Greek Groop',
        salary: 15000,
        birthday: new Date(1985, 2, 3),
        married: false,
        phones: ['087xxxxxx']
    }]
    Emp.insertMany(docs, err => {
        if(!err) {
            console.log('doc3 saved');
        }
    })
}
// module.exports.create = create
// module.exports.save = save
// module.exports.insertMany = insertMany