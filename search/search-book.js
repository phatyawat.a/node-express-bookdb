const Book = require('../model/model');

function searchBook(request, response) {

    let keyword = request.query.keyword
    let type = Number(request.query.type);
    let searchKey = new RegExp(keyword)

    let cond = [{ company: { $regex: searchKey }},
                { name: { $regex: searchKey }},
                { author: { $regex: searchKey }}]


    let bookList = Book.find({ $or: cond }, { _id: 0 })


    if(type != 0){
        bookList = bookList.and({ type:{ $eq : type} })
    }            

    bookList.select().exec((err, docs) => {

        if (!err) {

            if(keyword.length > 0){
                getResult(docs, keyword, response); 
            }else{
                response.setHeader('Content-Type', 'application/json');
                response.end(JSON.stringify(docs))
            }
                   
        }


    })

    

}

function getResult(docs, keyword, response) {

    let search = []

    for (d of docs) {

        if (keyword.length != 0) {
            search = addMatchItem(d, search, keyword)
        }

    }

    search.sort(function (a, b) {
        if (a < b) {
            return -1;
        } else if (a > b) {
            return 1;
        } else {
            return 0;
        }
    });

    response.setHeader('Content-Type', 'application/json');
    response.end(JSON.stringify(search))

}

function addMatchItem(d, search, keyword) {

    let company = d.company
    let name = d.name
    let author = d.author
    let dup = search.indexOf(d);

    if (company.includes(keyword) || name.includes(keyword) || author.includes(keyword)) {
        if (checkNotDup(dup)) {
            search.push(d)
        }
    }

    return search
}


function checkNotDup(d) {
    return d == -1
}

module.exports = searchBook