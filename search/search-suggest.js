const Book = require('../model/model');

function searchSuggest(request, response) {

    let keyword = request.query.keyword
    let type = request.query.type
    let searchKey = new RegExp(keyword)

    console.log(request.query);
    let t;

    let cond = [{ company: { $regex: searchKey } }, { name: { $regex: searchKey } }, { author: { $regex: searchKey } }]
 
    if(type != 0){
      t =  Book.find({ $or: cond }).and({type: { $eq: type }})
    }else{
      t =  Book.find({ $or: cond })
    }               
    
    t.select('name author company')
        .exec((err, docs) => {

            if (!err) {

                let search = []
                let result = []

                for (d of docs) {

                    let company = d.company
                    let name = d.name
                    let author = d.author

                    if (search.indexOf(company) == -1 && company.includes(keyword)) {
                        search.push(
                            {
                                text: company,
                                rate: company.indexOf(keyword)
                            }
                            
                        )
                    }
                    if (search.indexOf(name) == -1 && name.includes(keyword)) {
                        search.push(
                            {
                                text: name,
                                rate: name.indexOf(keyword)
                            }
                        )
                    }
                    if (search.indexOf(author) == -1 && author.includes(keyword)) {
                        search.push(
                            {
                                text: author,
                                rate: author.indexOf(keyword)
                            }
                        )
                    }
                   
                }

                search.sort(function (a, b) {
                    if (a.rate < b.rate) {
                        return -1;
                    } else if (a.rate > b.rate) {
                        return 1;
                    } else {
                        return 0;
                    }
                });
                for (s of search) {
                    if (result.indexOf(s.text) == -1){
                        result.push(s.text)
                    }
                    
                }

                response.setHeader('Content-Type', 'application/json');
                response.end(JSON.stringify(result))
            }

        })
}

module.exports = searchSuggest