const express = require('express')
const cors = require('cors');
const app = express()
const router = express.Router()
const searchSuggest = require('./search/search-suggest')
const searchBook = require('./search/search-book')

const corsOptions = {
   origin: 'http://localhost:4200',
   credentials: true,
 };

router.get('/searchSuggest', (request, response) => {
    searchSuggest(request, response)
 })

 router.get('/searchBook', (request, response) => {
    searchBook(request, response)
 })

app.use(cors(corsOptions));
app.use(router).listen(3000)
